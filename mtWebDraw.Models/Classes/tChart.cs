﻿using System;

namespace mtWebDraw.Models
{
    /// <summary>
    /// Web图基本信息表
    /// </summary>
    public class tChart
    {
    
        /// <summary>
        /// 标识
        /// </summary>
        public virtual Guid ID { get; set; }
        
        /// <summary>
        /// 名称
        /// </summary>
        public virtual String FullName { get; set; }
        
        /// <summary>
        /// 介绍
        /// </summary>
        public virtual String Caption { get; set; }
        
        /// <summary>
        /// 所有者
        /// </summary>
        public virtual Guid Owner { get; set; }
        
        /// <summary>
        /// 是否删除
        /// </summary>
        public virtual Boolean IsDelete { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual DateTime CreateTime { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public virtual DateTime UpdateTime { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        public virtual Guid UpdateUser { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public virtual String Remark { get; set; }
        
    }
}