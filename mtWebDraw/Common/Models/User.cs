﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mtWebDraw.Common
{
    [Serializable]
    public class User
    {

        /// <summary>
        /// 标识
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public String UserName { get; set; }

        /// <summary>
        /// 是否为管理员
        /// </summary>
        public Boolean IsAdmin { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public String FullName { get; set; }

    }
}